Categories:Games
License:GPLv2+
Web Site:https://github.com/phikal/ReGeX/blob/HEAD/README.md
Source Code:https://github.com/phikal/ReGeX
Issue Tracker:https://github.com/phikal/ReGeX/issues
Bitcoin:1Bu4r4UpcWYbivcMdLWJW9MGkciCJAC5ab

Auto Name:ReGeX
Summary:A regular expression matching game
Description:
Try and find a regular expression (Perl style) to match certain strings, while
not matching others.
.

Repo Type:git
Repo:https://github.com/phikal/ReGeX

Build:1.0,1
    commit=v1.0
    subdir=app
    gradle=yes

Build:1.1,2
    commit=v1.1
    subdir=app
    gradle=yes

Build:1.2,3
    commit=v1.2
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.2
Current Version Code:3
