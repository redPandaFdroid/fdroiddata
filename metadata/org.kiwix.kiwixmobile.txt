Categories:Internet
License:GPLv3
Web Site:http://www.kiwix.org
Source Code:http://sourceforge.net/projects/kiwix
Issue Tracker:http://sourceforge.net/p/kiwix/bugs

Auto Name:Kiwix
Summary:Offline Wikipedia reader
Description:
Kiwix lets you read Wikipedia and other websites without an Internet connection.
It uses the highly compressed [http://www.openzim.org/ ZIM format] to store
webpages for quick and easy reading.
.

Repo Type:git
# Repo:git://git.code.sf.net/p/kiwix/kiwix
Repo:https://github.com/kiwix/kiwix_mirror.git

Build:1.96,26
    commit=android_v1.96
    subdir=android
    gradle=yes
    rm=ios,dvd,src/sugar,android/libs/*/*.so
    build=cd .. && \
        ./autogen.sh && \
        ./configure --disable-staticbins --disable-manager --disable-server --disable-launcher --disable-indexer --disable-installer --disable-searcher --disable-reader --disable-components --enable-android --enable-compileall && \
        pushd src/dependencies && \
        make icudt49l.dat xz icu zimlib-1.2 zimlib-1.2/build/lib/libzim.so && \
        popd && \
        cd android && \
        NDK_PATH=$$NDK$$ ./build-android-with-native.py --locales --toolchain --lzma --icu --zim --kiwix --strip

Maintainer Notes:
Don't run `make android-deps` as that will download the SDK and NDK.

Use GitHub repo mirror as the sf one fails to clone. Also, sf is much slower.
.

Auto Update Mode:None
Update Check Mode:Tags android_*
Current Version:1.96
Current Version Code:26
